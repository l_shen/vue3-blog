#### 下载依赖

`npm install`

#### 运行项目

`npm run dev`

#### 打包项目

`npm run build`


#### 项目介绍

此博客项目纯属个人练习用，技术栈：Vite +  Vue3 全家桶 + 组合式Api + Pinia + element-plus + halo Api（用于博客数据）+ 网易云音乐Api（音乐盒） 
