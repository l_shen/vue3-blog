import { createApp } from 'vue'

import router from './router'
import pinia from './stores'
import App from './App.vue'

import '@/utils/useCanvasBg'
import 'normalize.css'
import './assets/css/index.css'
import 'element-plus/theme-chalk/display.css'
import 'element-plus/theme-chalk/dark/css-vars.css'

import * as ElementPlusIconsVue from '@element-plus/icons-vue'
// markdown插件
import VMdPreview from '@/utils/md-edit.js'

const app = createApp(App)
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}
app.use(router).use(pinia).use(VMdPreview).mount('#app')
