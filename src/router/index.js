import { createRouter, createWebHashHistory } from 'vue-router'
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'

const routes = [
  {
    path: '/',
    redirect: '/home'
  },
  {
    path: '/home',
    component: () => import('@/views/home/home.vue'),
    meta: {
      activeIndex: '0'
    }
  },
  {
    path: '/categories',
    component: () => import('@/views/categories/categories.vue'),
    meta: {
      activeIndex: '1'
    }
  },
  {
    path: '/archives',
    component: () => import('@/views/archives/archives.vue'),
    meta: {
      activeIndex: '2'
    },
    children: [
      {
        path: 'article',
        component: () => import('@/views/article/article.vue')
      }
    ]
  },
  {
    path: '/profile',
    component: () => import('@/views/profile/profile.vue'),
    meta: {
      activeIndex: '3'
    }
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes,
  // scrollBehavior(to, from, savedPosition) {
  //   console.log(savedPosition)
  //   if (savedPosition) {
  //     return savedPosition
  //   } else {
  //     return {
  //       x: 0,
  //       y: 0
  //     }
  //   }
  // }
})
// 前置守卫
router.beforeEach((to, form) => {
  NProgress.start()
})
// 后置守卫
router.afterEach(() => {
  NProgress.done()
  window.scrollTo({
    top: 0,
    left: 0,
    behavior: 'smooth'
  })
})

export default router