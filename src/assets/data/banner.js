const bannerChatData = [
  {
    id: 1,
    img: 'banner/weixin.png',
    content: '843567305'
  },
  {
    id: 2,
    img: 'banner/qq.png',
    content: '843567305'
  },
  {
    id: 3,
    img: 'banner/gitee.png',
    content: 'https://gitee.com/l_shen/dashboard/projects'
  }
]

export default bannerChatData