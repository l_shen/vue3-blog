import axios from 'axios'
import { BASE_URL, TIMEOUT } from './config'

class MyRequest {
  constructor(baseURL, timeout = 10000) {
    this.instance = axios.create({
      baseURL,
      timeout
    })

    this.instance.interceptors.request.use((config) => {
      return config
    }, err => {
      return err
    })

    this.instance.interceptors.response.use(res => {
      return res
    }, err => {
      return err
    })
  }

  request(config) {
    return new Promise((resolve, reject) => {
      this.instance.request(config).then(res => {
        resolve(res.data)
      }).catch(err => {
        reject(err)
      })
    })
  }

  get(config) {
    return this.request({ ...config, method: "get" })
  }

  post(config) {
    return this.request({ ...config, method: "post" })
  }
}

export const myRequest = new MyRequest(BASE_URL)





import { ContentApiClient, HaloRestAPIClient } from '@halo-dev/content-api'

// 环境切换
let baseUrl = 'http://124.221.193.177:8090';
const Accesskey = '123456'
const haloRestApiClient = new HaloRestAPIClient({
  baseUrl: baseUrl,
  auth: { apiToken: Accesskey },
})

export default new ContentApiClient(haloRestApiClient)



