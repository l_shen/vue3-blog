import request from '../request/request'
// 文章归档
export function getArchiveData() {
  return request.archive.listMonthArchives()
}