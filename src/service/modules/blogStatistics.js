import request from '../request/request'

// 博客统计数据
export function getStatisticsData() {
  return request.statistic.statistics()
}

// 博客统计数据and用户信息
export function getStatisticsWithUserData() {
  return request.statistic.statisticsWithUser
}
