import { myRequest } from '../request/request'

// 获取巅峰榜数据
export function getTopList() {
  return myRequest.get({ url: '/toplist/detail' })
}

// 通过id获取巅峰榜详情
export function getTopListDetail(id) {
  return myRequest.get({
    url: '/playlist/detail', params: {
      id
    }
  })
}

// 通过id获取当前歌曲详情
export function getMusicInfo(ids) {
  return myRequest.get({
    url: '/song/detail', params: {
      ids
    }
  })
}

// 获取歌曲播放地址 url
export function getSongUrlById(id) {
  return myRequest.get({
    url: '/song/url', params: {
      id
    }
  })
}