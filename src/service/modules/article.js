import request from '../request/request'

export function getArticleDetailData(postId) {
  return request.post.get(postId)
}
