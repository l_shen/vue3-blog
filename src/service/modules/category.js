import request from '../request/request'

export function getCategoryData() {
  return request.category.list({ more: true, sort: 'priority' })
  // return request.category.listPostBySlug()
}

export function getCategoryBySlug(slug = 'default', page = 0, size = 5, sort, password) {
  return request.category.listPostBySlug({ slug, page, size })
}