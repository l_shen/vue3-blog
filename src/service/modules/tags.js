import request from '../request/request'

// 获取标签
export function getTagsListData() {
  return request.tag.list()
}

// 通过slug获取标签列表
export function getTagsListBySlugData() {
  return request.tag.listPostsBySlug
}