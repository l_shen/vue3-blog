import request from '../request/request'

export function getUserProfileData() {
  return request.user.getProfile()
}