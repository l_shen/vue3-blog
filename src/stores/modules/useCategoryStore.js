import { defineStore } from 'pinia'
import { getCategoryData, getCategoryBySlug } from '@/service/modules/category'

const categoryStore = defineStore('category', {
  state: () => {
    return {
      categoryData: [],
      categoryBySlugData: {
      }
    }
  },
  actions: {
    async fetchCategoryData() {
      const res = await getCategoryData()
      // console.log(res)
      this.categoryData = res.data
    },
    async fetchCategoryBySlugData(slug, page, size) {
      const res = await getCategoryBySlug(slug, page, size)
      // console.log(res)
      this.categoryBySlugData = res.data
    }
  }
})

export default categoryStore