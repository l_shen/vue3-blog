import { defineStore } from 'pinia'
import { getTopList, getTopListDetail, getMusicInfo, getSongUrlById } from '@/service/modules/music'

const musicStore = defineStore('music', {
  state: () => {
    return {
      musicTotListData: [],
      playListDataTracks: [],
      currentSongName: '',
      currentSongUrl: '',
      currentSongIndex: 0,

    }
  },
  actions: {
    async fetchMusicTopListData() {
      const res = await getTopList()
      this.musicTotListData = res.list
    },
    async fetchMusicTopListDetailData(id = 3778678) {
      const res = await getTopListDetail(id)
      this.playListDataTracks = res.playlist.tracks
      // setTimeout(() => {
      //   this.playListDataTracks.forEach((item, index) => {
      //     if (this.currentSongIndex === index) {
      //       this.currentSongName = item.name
      //       this.fetchSongUrlData(item.id)
      //     }
      //   })
      // }, 2000)
    },
    async getSongInfos() {
      this.playListDataTracks.forEach((item, index) => {
        if (this.currentSongIndex === index) {
          this.currentSongName = item.name
          this.fetchSongUrlData(item.id)
        }
      })
    },
    async fetchSongUrlData(id) {
      const res = await getSongUrlById(id)
      this.currentSongUrl = res.data[0].url
    }
  }
})

export default musicStore