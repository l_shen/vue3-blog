import { defineStore } from 'pinia'
import { getArticleDetailData } from '@/service/index'

const articleStore = defineStore('article', {
  state: () => {
    return {
      articleDetailData: {}
    }
  },
  actions: {
    async fetchArticleDetailData(id) {
      const res = await getArticleDetailData(id)
      console.log(res)
      this.articleDetailData = res.data
    }
  }
})

export default articleStore