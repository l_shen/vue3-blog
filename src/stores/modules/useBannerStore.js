import { defineStore } from 'pinia'

const bannerStore = defineStore('banner', {
  state: () => {
    return {
      bannerValue: ''
    }
  }
})

export default bannerStore