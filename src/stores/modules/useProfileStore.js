import { defineStore } from 'pinia'
import { getUserProfileData } from '@/service/index'

const profileStore = defineStore('profile', {
  state: () => {
    return {
      userProfileData: {}
    }
  },
  actions: {
    async fetchUserProfileData() {
      const res = await getUserProfileData()
      this.userProfileData = res.data
    }
  }
})

export default profileStore