import { defineStore } from 'pinia'
import { getArchiveData } from '@/service'

const archiveStore = defineStore('archive', {
  state: () => {
    return {
      archiveData: []
    }
  },
  actions: {
    async fetchArchiveData() {
      const res = await getArchiveData()
      console.log(res)
      this.archiveData = res.data
    }
  }
})

export default archiveStore