const getAssetsURL = (url) => {
  // 参数1，相对路径 ， 参数2，当前路径的URL
  return new URL(`../assets/images/${url}`, import.meta.url).href
}

export default getAssetsURL